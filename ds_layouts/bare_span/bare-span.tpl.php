<?php
/**
 * @file
 * Display Suite Bare Span template.
 * 
 * Provides a Simple Span for DS content to render into.
 *
 * Available variables:
 *
 * Layout:
 * - $classes: String of classes that can be used to style this layout.
 * - $contextual_links: Renderable array of contextual links.
 *
 * Regions:
 *
 * - $span: Rendered content for the "Span" region.
 * - $span_classes: String of classes that can be used to style the "Span" region.
 */
?>
<span class="<?php print $classes; ?>"><?php print $span; ?></span>