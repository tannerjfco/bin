<?php

/**
 * @file
 * Display Suite Bare Span configuration.
 */

function ds_bare_span() {
  return array(
    'label' => t('Bare Span'),
    'regions' => array(
      'span' => t('Span'),
    ),
    // Uncomment if you want to include a CSS file for this layout (bare_span.css)
    // 'css' => TRUE,
    // Uncomment if this is a template for a node form.
    // 'form' => TRUE,
  );
}
